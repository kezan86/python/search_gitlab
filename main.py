import gitlab
import yaml
import sys
from pprint import pprint

dir_common_ci = 'common-ci'
douload_file = '/tmp/raw-download.yaml'
branch = 'master'
variables = 'variables'
werf_version = 'WERF_VERSION'
common_file = ['multiStage.yml',
               'multiStage1.2.yaml',
               'multiStageInfra.yml',
               'multiStageInfra1.2.yml',
               'multiStageMultiBuild.yml',
               'multiStagev2.1.2.yaml',
               'multiStagev2.1.2.yaml',
               'singleStage.yml',
               'singleStageInfra.yml',
               'singleStageInfra1.2.yml']

ok = {}
ok_import = {}
err_var = {}
err_file = {}

# Авторизация
gl = gitlab.Gitlab(url=sys.argv[1], private_token=sys.argv[2], http_username=sys.argv[3], http_password=sys.argv[4])
gl.auth()
projects = gl.projects.list(all=True)

def proverka(file):
    # Получить содержимое файла
    try:
        raw_content = project.files.raw(file_path=file, ref=branch)
        with open(douload_file, 'wb') as f:
            project.files.raw(file_path=file, ref=branch, streamed=True, action=f.write)
        # Парсинг Yaml файла
        with open(douload_file, 'r') as stream:
            text = yaml.safe_load(stream)
            try:
                ver = text[variables][werf_version]
                #print(f'Проект: {project_name}. Ссылка на проект: {project_url}. Файл: {file}. Канал обновления: {ver}')
                ok[project_name]=[{'URL':project_url}, {'File':file}, {'Vesion':ver}]
            except KeyError as key:
                try:
                    ver = text['include'][0]['file']
                    #print(f'Проект: {project_name}. Ссылка на проект: {project_url}. Файл импортирован - {ver}')
                    ok_import[project_name]=[{'URL':project_url}, {'File import':ver}]
                except KeyError as key:
                    pass
                    #print(f'Проект: {project_name}. Ссылка на проект: {project_url}. Нету ключа {werf_version}')
                    err_var[project_name]=[{'URL':project_url}, {'Values error':werf_version}]
    except gitlab.exceptions.GitlabGetError as exc:
        pass
        #print(f'Проект: {project_name}. Ссылка на проект: {project_url}. Файл {file} не найден')
        err_file[project_name]=[{'URL':project_url}, {'File not found':file}]

for project in projects:
    project = gl.projects.get(project.encoded_id)
    info = project.attributes

    project_name = info['name']
    project_url = info['http_url_to_repo']

    if project_name == dir_common_ci:
        for common in common_file:
            file = common
            project_name = file
            proverka(file)
    else:
        file = '.gitlab-ci.yml'
        proverka(file)

print("Успешно найденные файлы:")
pprint(ok)
print()
print()

print("Проекты где файл импортирован:")
pprint(ok_import)
print()
print()

print("Проекты где нет переменной WERF_VERSION:")
pprint(err_var)
print()
print()

print("Проекты где нет файла CI:")
pprint(err_file)
print()
print()
